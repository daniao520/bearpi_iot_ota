#ifndef __E53_IA1_H_
#define __E53_IA1_H_

#include "main.h"
#include "usart.h"
#include "stdio.h"
#include "string.h"
#include <ctype.h>
#include <stdlib.h>
#include "i2c.h"


#define    BH1750_ADDR_WRITE    0x46    //01000110
#define    BH1750_ADDR_READ    0x47    //01000111
#define    SHT30_ADDR_WRITE    0x44<<1         //10001000
#define    SHT30_ADDR_READ        (0x44<<1)+1        //10001011

#define CRC8_POLYNOMIAL 0x31

extern uint16_t LED_Toggle_time;

typedef enum
{
    POWER_OFF_CMD    =    0x00,    //断电：无激活状态
    POWER_ON_CMD    =    0x01,    //通电：等待测量指令
    RESET_REGISTER    =    0x07,    //重置数字寄存器（在断电状态下不起作用）
    CONT_H_MODE        =    0x10,    //连续H分辨率模式：在11x分辨率下开始测量，测量时间120ms
    CONT_H_MODE2    =    0x11,    //连续H分辨率模式2：在0.51x分辨率下开始测量，测量时间120ms
    CONT_L_MODE        =    0x13,    //连续L分辨率模式：在411分辨率下开始测量，测量时间16ms
    ONCE_H_MODE        =    0x20,    //一次高分辨率模式：在11x分辨率下开始测量，测量时间120ms，测量后自动设置为断电模式
    ONCE_H_MODE2    =    0x21,    //一次高分辨率模式2：在0.51x分辨率下开始测量，测量时间120ms，测量后自动设置为断电模式
    ONCE_L_MODE        =    0x23    //一次低分辨率模式：在411x分辨率下开始测量，测量时间16ms，测量后自动设置为断电模式
} BH1750_MODE;

typedef enum
{
    /* 软件复位命令 */

    SOFT_RESET_CMD = 0x30A2,    
    /*
    单次测量模式
    命名格式：Repeatability_CS_CMD
    CS： Clock stretching
    */
    HIGH_ENABLED_CMD    = 0x2C06,
    MEDIUM_ENABLED_CMD  = 0x2C0D,
    LOW_ENABLED_CMD     = 0x2C10,
    HIGH_DISABLED_CMD   = 0x2400,
    MEDIUM_DISABLED_CMD = 0x240B,
    LOW_DISABLED_CMD    = 0x2416,

    /*
    周期测量模式
    命名格式：Repeatability_MPS_CMD
    MPS：measurement per second
    */
    HIGH_0_5_CMD   = 0x2032,
    MEDIUM_0_5_CMD = 0x2024,
    LOW_0_5_CMD    = 0x202F,
    HIGH_1_CMD     = 0x2130,
    MEDIUM_1_CMD   = 0x2126,
    LOW_1_CMD      = 0x212D,
    HIGH_2_CMD     = 0x2236,
    MEDIUM_2_CMD   = 0x2220,
    LOW_2_CMD      = 0x222B,
    HIGH_4_CMD     = 0x2334,
    MEDIUM_4_CMD   = 0x2322,
    LOW_4_CMD      = 0x2329,
    HIGH_10_CMD    = 0x2737,
    MEDIUM_10_CMD  = 0x2721,
    LOW_10_CMD     = 0x272A,
    /* 周期测量模式读取数据命令 */
    READOUT_FOR_PERIODIC_MODE = 0xE000,
} SHT30_CMD;


uint16_t BH1750_Dat_To_Lux(uint8_t* dat);
uint8_t BH1750_Read_Dat(uint8_t* dat);
uint8_t  BH1750_Send_Cmd(BH1750_MODE cmd);
uint8_t CheckCrc8(uint8_t* const message, uint8_t initial_value);
uint8_t SHT30_Dat_To_Float(uint8_t* const dat, float* temperature, float* humidity);
uint8_t SHT30_Read_Dat(uint8_t* dat);
uint8_t SHT30_Init(void);
void SHT30_Reset(void);
static uint8_t SHT30_Send_Cmd(SHT30_CMD cmd);


#endif



